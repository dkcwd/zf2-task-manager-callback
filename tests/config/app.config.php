<?php

return array(
    'service_manager' => array(
        'factories' => array(
            'Zf2TaskManager\Listener\TaskListener' => 'Zf2TaskManager\Listener\Service\TaskListenerFactory',
        ),
        'invokables' => array(
            'ExampleService' => 'Zf2TaskManagerCallbackTestAsset\Service\ExampleService',
        ),
    ),
    'task_manager' => array(
        /**
         * Example configuration, use the execute strategy until you are ready to use queueing
         */
        'strategy' => \Zf2TaskManager\Listener\Service\TaskListenerFactory::EXECUTE,
    ),
);
