<?php

namespace Zf2TaskManagerCallback\Task\Service;

use SlmQueue\Job\AbstractJob;

use Zf2TaskManagerCallback\Task\CallbackTask;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class CallbackTaskFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $task = new CallbackTask();
        return $task;
    }
}
