<?php

namespace Zf2TaskManagerCallback;

use Zend\EventManager\EventInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

class Module implements ConfigProviderInterface
{
    public function getConfig()
    {
        defined('ZF2_TASK_MANAGER_CALLBACK_TEST') || define('ZF2_TASK_MANAGER_CALLBACK_TEST', false);

        return array(
            'slm_queue' => array(
                'job_manager' => array(
                    'factories' => array(
                        'Zf2TaskManagerCallback\Task\CallbackTask' => 'Zf2TaskManagerCallback\Task\Service\CallbackTaskFactory',
                    ),
                ),
            ),
            'task_manager' => array(
                'registered_tasks' => array(
                    'task-callback' => 'Zf2TaskManagerCallback\Task\CallbackTask',
                )
            ),
        );
    }
}
