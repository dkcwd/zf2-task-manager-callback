<?php

namespace Zf2TaskManagerCallbackTestAsset\Service;

use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;

class ExampleService implements EventManagerAwareInterface
{
    use EventManagerAwareTrait;

    public function triggerEvent($event)
    {
        return $this->getEventManager()->trigger($event);
    }

}