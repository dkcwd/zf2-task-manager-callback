<?php

namespace Zf2TaskManagerCallback\Task;

use SlmQueue\Job\AbstractJob;

use Zend\Stdlib\Exception\RuntimeException;
use Zend\Stdlib\Exception\LogicException;

class CallbackTask extends AbstractJob
{
    /**
     * Execute the job
     */
    public function execute()
    {
        $content = $this->getContent();
        if (! isset($content['task-callback'])) {
            throw new \Exception(
                'A callback task requires configuration parameters to be passed via the task-callback index'
            );
        }

        $config = $content['task-callback'];
        if (isset($config['url'])) {
            // TODO add validation, logging hooks, optional arguments, curl support and specific http method config
            $result = file_get_contents($config['url']);
            // TODO revise solution to test url call as it does not test actual url callback yet
            if (ZF2_TASK_MANAGER_CALLBACK_TEST) {
                file_put_contents(
                    __DIR__ . '../../../../tests/Zf2TaskManagerCallbackTestAsset/test-url.content',
                    $config['url'] . date('YmdHis'));
            }
            return $result;
        } elseif (isset($config['class'])) {
            // TODO revise solution to test url call as it does not test actual class callback yet
            if (ZF2_TASK_MANAGER_CALLBACK_TEST) {
                file_put_contents(
                    __DIR__ . '../../../../tests/Zf2TaskManagerCallbackTestAsset/test-class.content',
                    $config['class'] . date('YmdHis'));
            }
            return;
        } else {
            throw new \Exception(
                'No valid callback options were supplied'
            );
        }

    }

}
