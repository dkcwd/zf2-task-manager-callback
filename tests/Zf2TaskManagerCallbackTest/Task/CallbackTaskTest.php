<?php

namespace Zf2TaskManagerCallbackTest\Event;

use \PHPUnit_Framework_TestCase;

use Zend\Test\Util\ModuleLoader;
use Zf2TaskManager\Event\TaskEvent;
use Zf2TaskManagerCallback\Task\CallbackTask;

class CallbackTaskTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    public function setUp()
    {
        $loader = new ModuleLoader(array(
            'modules' => array(
                'SlmQueue' => new \SlmQueue\Module(),
                'Zf2TaskManager' => new \Zf2TaskManager\Module(),
                'Zf2TaskManagerCallback' => new \Zf2TaskManagerCallback\Module(),
            ),
            'module_listener_options' => array(
                'config_static_paths' => array(
                    __DIR__ . '/../../config/app.config.php',
                ),
            ),
        ));

        $this->serviceLocator = $loader->getServiceManager();
        $this->serviceLocator->setAllowOverride(true);
        $this->serviceLocator->get('EventManager')->attach(
            $this->serviceLocator->get('Zf2TaskManager\Listener\TaskListener')
        );
    }

    public function testTaskUrlCallbackCanBeExecutedViaListener()
    {
        $this->serviceLocator->get('ExampleService')->triggerEvent(
            new TaskEvent(
                'task-callback',
                '*',
                array(
                    'task-callback' => array(
                        'url' => 'https://www.google.com.au',
                    ),
                )
            )
        );

        /**
         * TODO: determine a better way of implementing this test and do it
         */
        $string = file_get_contents(__DIR__ . '/../../Zf2TaskManagerCallbackTestAsset/test-url.content');
        $this->assertStringStartsWith(
            'https://www.google.com.au' . date('YmdHis'),
            $string
        );
    }

    public function testTaskClassCallbackCanBeExecutedViaListener()
    {
        $this->serviceLocator->get('ExampleService')->triggerEvent(
            new TaskEvent(
                'task-callback',
                '*',
                array(
                    'task-callback' => array(
                        'class' => '\SomeNamespace\Class'
                    ),
                )
            )
        );

        /**
         * TODO: determine a better way of implementing this test and do it
         */
        $string = file_get_contents(__DIR__ . '/../../Zf2TaskManagerCallbackTestAsset/test-class.content');
        $this->assertStringStartsWith(
            '\SomeNamespace\Class' . date('YmdHis'),
            $string
        );
    }

    public function testExceptionThrownWhenExecutedViaListenerWithInvalidCallbackOptions()
    {
        $this->setExpectedException(
            'Exception',
            'No valid callback options were supplied'
        );

        $this->serviceLocator->get('ExampleService')->triggerEvent(
            new TaskEvent(
                'task-callback',
                '*',
                array(
                    'task-callback' => array(
                        'invalid_key' => 'https://www.google.com.au',
                    ),
                )
            )
        );
    }

    public function testExceptionThrownWhenExecutedViaListenerWithNoCallbackConfigurationOptions()
    {
        $this->setExpectedException(
            'Exception',
            'A callback task requires configuration parameters to be passed via the task-callback index'
        );

        $this->serviceLocator->get('ExampleService')->triggerEvent(
            new TaskEvent(
                'task-callback',
                '*',
                array(
                )
            )
        );
    }

}